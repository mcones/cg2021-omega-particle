#include "helper.hpp"

auto get_filedate(const std::string& filename) {
    struct stat result;
    __time64_t time = 0;
    if (stat(filename.c_str(), &result) == 0) {
        return result.st_mtime;
    }
    else return time;